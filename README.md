# leo-ui-auto-java

#### 介绍

「Leo-UI-Auto」是一个免费开源的UI自动化测试框架。

1. 此框架主要用于浏览器 网页UI自动化测试，使用Java语言
2. 基于PageObject模型，将元素定位、业务逻辑、测试数据分离
3. 支持yaml文件、excel文件 读取数据
4. 框架使用Allure生成测试报告


#### 软件架构

Java + Maven + TestNG + Selenium + Allure.


#### 使用说明

1. xxxx
2. xxxx
3. xxxx