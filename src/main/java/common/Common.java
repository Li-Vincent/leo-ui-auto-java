package common;


/**
 * 公共方法
 *
 * @author Vincent-Li
 */
public class Common {

    /**
     * 线程等待
     *
     * @param milliSecond int 毫秒
     */
    public static void sleep(int milliSecond) {
        try {
            Thread.sleep(milliSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
