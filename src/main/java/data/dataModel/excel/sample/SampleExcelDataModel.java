package data.dataModel.excel.sample;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SampleExcelDataModel extends ExcelBaseModel {
    // 继承BaseDataModel, index = 0 /1 被占用，从2开始
    @ExcelProperty(value = "姓名", index = 2)
    private String name;

    @ExcelProperty(value = "年龄", index = 3)
    private int age;

    @ExcelProperty(value = "昵称", index = 4)
    private String nickname;

    @ExcelProperty(value = "密码", index = 5)
    private String password;

    @ExcelProperty(value = "手机", index = 6)
    private String phone;

    @ExcelProperty(value = "邮箱", index = 7)
    private String email;

    @ExcelProperty(value = "城市", index = 8)
    private String city;

    @ExcelProperty(value = "性别", index = 9)
    private String gender;

    @ExcelProperty(value = "身高", index = 10)
    private String height;

    @ExcelProperty(value = "备注", index = 11)
    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static Map<String, SampleExcelDataModel> read(String filePath) {
        // map<key, object> key = testName+scenario
        final Map<String, SampleExcelDataModel> dataMap = new ConcurrentHashMap<>();
        try (InputStream in = new FileInputStream(filePath);) {
            AnalysisEventListener<SampleExcelDataModel> listener = new AnalysisEventListener<SampleExcelDataModel>() {

                @Override
                public void invoke(SampleExcelDataModel data, AnalysisContext context) {
                    dataMap.put(data.getTestName() + data.getScenario(), data);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext context) {
                }
            };
            EasyExcel.read(in, SampleExcelDataModel.class, listener).sheet(0).doRead();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataMap;
    }
}
