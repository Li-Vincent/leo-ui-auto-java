package data.dataModel.yaml.sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import common.utils.LeoLogger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class SampleYamlData {
    private List<SampleYamlData.SampleModel> testCases;

    public List<SampleYamlData.SampleModel> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<SampleYamlData.SampleModel> testCases) {
        this.testCases = testCases;
    }

    public static SampleModel getDataByScenario(SampleYamlData dataList, String testName, String scenarioName) {
        for (SampleModel data : dataList.getTestCases()) {
            if (data.getTestName().trim().equals(testName)
                    && data.getScenarioName().trim().equals(scenarioName)) {
                return data;
            }
        }
        return null;
    }

    public static SampleYamlData getAllDataFromFile(String filePath) {
        try {
            ObjectMapper dataMap = new ObjectMapper(new YAMLFactory());
            return dataMap.readValue(new File(filePath), SampleYamlData.class);
        } catch (IOException e) {
            LeoLogger.error("Read file failed, filePath: " + filePath, e);
        }
        return null;
    }


    public static class SampleModel {
        private String testName;
        private String scenarioName;
        private String name;
        private int age;
        private String nickname;
        private String password;
        private String phone;
        private String email;
        private String city;

        public String getTestName() {
            return testName;
        }

        public void setTestName(String testName) {
            this.testName = testName;
        }

        public String getScenarioName() {
            return scenarioName;
        }

        public void setScenarioName(String scenarioName) {
            this.scenarioName = scenarioName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }
}
