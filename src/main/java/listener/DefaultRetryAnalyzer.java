package listener;


import common.utils.ConfUtils;
import common.utils.LeoLogger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

/**
 * Customize IRetryAnalyzer, 失败重跑机制
 *
 * @author Vincent-Li
 */
public class DefaultRetryAnalyzer implements IRetryAnalyzer {
    private int retryCount = 1;
    private static final int maxRetryCount = ConfUtils.getMaxRerunTimes(); // 控制失败跑几次

    @Override
    synchronized public boolean retry(ITestResult result) {
        try {
            Throwable failException = result.getThrowable();
            if (failException instanceof AssertionError) {
                LeoLogger.setTestStep("assert Error, no need retry");
            } else {
                if (retryCount < maxRetryCount) {
                    LeoLogger.setTestStep("Will rerun Test - " + result.getName() + " - Times:" + retryCount);
                    Reporter.setCurrentTestResult(result);
                    retryCount++;
                    return true;
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        LeoLogger.setTestStep("Test Failed - " + result.getMethod().getMethodName());
        return false;
    }
}