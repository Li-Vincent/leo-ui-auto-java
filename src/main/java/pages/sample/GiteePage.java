package pages.sample;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import pages.BasePage;

public class GiteePage extends BasePage {
    public GiteePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "navbar-search-input")
    public WebElement navbar_search_input;

    @FindBy(how = How.XPATH, using = "//*[@id='navbar-search-form']/div[2]/div/a[1]")
    public WebElement navbar_search_all_link;

    @FindBy(how = How.XPATH, using = "//*[@id='hits-list']/div[1]/div[1]/div/a")
    public WebElement search_result_1_title;


}
