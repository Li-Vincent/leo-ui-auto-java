package testScripts.sample.testBaseFlow;

import org.testng.annotations.Test;

@Test(groups = {"BaseFlow", "should-skip"})
public class DependsOnSkipFlowTest {

    @Test(dependsOnGroups = "should-fail")
    public void skippedDueToDependentGroup() {
        assert false : "This method is supposed to be skipped.";
    }


    @Test(dependsOnMethods = "skippedDueToDependentGroup")
    public void skippedDueToDependentMethod() {
        assert false : "This method is supposed to be skipped.";
    }
}
