package testScripts.sample.testBaseFlow;

import io.qameta.allure.Allure;
import listener.DefaultRetryAnalyzer;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import base.BaseFlow;
import common.utils.LeoLogger;

@Test(groups = {"BaseFlow", "should-fail"})
public class FailFlowTest extends BaseFlow {

    @Test(retryAnalyzer = DefaultRetryAnalyzer.class, description = "Fail Flow Test: exceptionThrown")
    public void exceptionThrown() {
        // Attach Test Case ID, 请将测试用例管理系统对应的case连接填入
        Allure.link("CaseID", "https://www.cnblogs.com/vincent-li666/p/14689449.html");
        String url = "https://www.baidu.com";
        // Mark Case Step
        LeoLogger.logCaseStep("open url " + url);
        driver.get(url);
        driver.findElement(By.id("kw")).sendKeys("leo-api-auto");
        driver.findElement(By.id("su")).click();
        LeoLogger.logCaseStep("search keyword leo-api-auto");
        throw new IllegalStateException("Test failed.", new UnsupportedOperationException());
    }

    @Test(description = "Fail Flow Test: assertionFailureWithOutput")
    public void assertionFailureWithOutput() {
        // Attach Test Case ID
        Allure.link("CaseID", "https://www.cnblogs.com/vincent-li666/p/14689449.html");
        String url = "https://www.sogou.com";
        // Mark Case Step
        LeoLogger.logCaseStep("open url " + url);
        driver.get(url);
        driver.findElement(By.id("query")).sendKeys("leo-api-auto");
        driver.findElement(By.id("stb")).click();
        LeoLogger.logCaseStep("search keyword leo-api-auto");
        assert false : "This test failed.\nIts message is on multiple lines.\n     The last one has leading whitespace.";
    }

}
