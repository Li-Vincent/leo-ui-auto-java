//=============================================================================
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=============================================================================
package testScripts.sample.testBaseFlow;

import base.BaseFlow;
import common.utils.LeoLogger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * A configuration failure for testing that the report correctly reports
 * configuration failures.
 *
 * @author Vincent-Li
 */
@Test(groups = {"BaseFlow", "should-skip"})
public class FailedConfigurationFlowTest extends BaseFlow {
    /**
     * A configuration method that will fail causing any test cases
     * in this class to be skipped.
     */
    @BeforeClass
    public void preCondition() {
        try {
            LeoLogger.logCaseStep("A configuration method that will fail causing any test cases in this class to be skipped.");
            throw new RuntimeException("Configuration failed.");
        } catch (RuntimeException e) {
            // To avoid interruption of the test process, exceptions should be caught and handled as much as possible
            // 如果不捕获异常，可能会导致@AfterTest/@AfterClass方法不能正常执行。
            Assert.fail("Config Exception: " + e.toString());
        }
    }

    /**
     * Because BeforeClass is fail, this method will be skipped.
     * in this class to be skipped.
     */
    @AfterClass
    public void postCondition() {
        if (driver != null){
            driver.quit();
        }
    }

    /**
     * This test ought to be skipped since the configuration for this
     * class will fail.
     */
    @Test
    public void thisShouldBeSkipped() {
        assert false : "This method is supposed to be skipped.";
    }
}
