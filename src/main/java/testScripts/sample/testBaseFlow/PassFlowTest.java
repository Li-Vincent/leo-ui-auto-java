package testScripts.sample.testBaseFlow;

import common.ScreenShot;
import io.qameta.allure.Allure;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import base.BaseFlow;
import common.utils.LeoLogger;

@Test(groups = {"BaseFlow", "should-pass"})
public class PassFlowTest extends BaseFlow {

    @BeforeMethod
    public void preCondition() {
        LeoLogger.logCaseStep("You can do some pre config in this method");
    }


    @Test(description = "Pass Flow Test: baiduSearchTest")
    public void baiduSearchTest() {
        // Attach Test Case ID, 请将测试用例管理系统对应的case连接填入
        Allure.link("CaseID", "https://www.cnblogs.com/vincent-li666/p/14689449.html");
        String url = "https://www.baidu.com";
        // Mark Case Step
        LeoLogger.logCaseStep("open url " + url);
        driver.get(url);
        driver.findElement(By.id("kw")).sendKeys("leo-api-auto");
        driver.findElement(By.id("su")).click();
        LeoLogger.logCaseStep("search keyword leo-api-auto");
        ScreenShot.attachScreenShotToAllure(driver);
    }

    @Test(description = "Pass Flow Test: sogouSearchTest")
    public void sogouSearchTest() {
        // Attach Test Case ID
        Allure.link("CaseID", "https://www.cnblogs.com/vincent-li666/p/14689449.html");
        String url = "https://www.sogou.com";
        // Mark Case Step
        LeoLogger.logCaseStep("open url " + url);
        driver.get(url);
        driver.findElement(By.id("query")).sendKeys("leo-api-auto");
        driver.findElement(By.id("stb")).click();
        LeoLogger.logCaseStep("search keyword leo-api-auto");
        ScreenShot.attachScreenShotToAllure(driver);
    }

    @AfterMethod
    public void postCondition() {
        LeoLogger.logCaseStep("You can do some rollback operation in this method");
    }
}
