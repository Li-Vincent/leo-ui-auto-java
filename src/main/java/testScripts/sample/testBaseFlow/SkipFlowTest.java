package testScripts.sample.testBaseFlow;

import base.BaseFlow;
import common.utils.LeoLogger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(groups = {"BaseFlow", "should-skip"})
public class SkipFlowTest extends BaseFlow {

    /**
     * A configuration method that will fail causing any test cases
     * in this class to be skipped.
     */
    @BeforeMethod
    public void configure() {
        try {
            LeoLogger.logCaseStep("A configuration method that will fail causing any test cases in this class to be skipped.");
            throw new IllegalStateException("Config failed.", new UnsupportedOperationException());
        } catch (Exception e) {
            // To avoid interruption of the test process, exceptions should be caught and handled as much as possible
            // 如果不捕获异常，可能会导致@AfterTest/@AfterClass方法不能正常执行。
            Assert.fail("Config Exception: " + e.toString());
        }
    }

    /**
     * This test ought to be skipped since the configuration for this
     * class will fail.
     */
    @Test(description = "this case should be skipped because failed config")
    public void thisShouldBeSkipped() {
        assert false : "This method is supposed to be skipped.";
    }

}
