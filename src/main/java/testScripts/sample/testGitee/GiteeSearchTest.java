package testScripts.sample.testGitee;

import base.BaseFlow;
import common.Common;
import common.ScreenShot;
import common.UIController;
import common.utils.LeoLogger;
import io.qameta.allure.Allure;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.sample.GiteePage;

/**
 * Sample Test Script - Gitee Search Test
 * <p>
 * Use Page Object Model
 * Use UIController
 *
 * @author Vincent-Li
 */
public class GiteeSearchTest extends BaseFlow {

    @Test()
    public void navbar_search() {
        Allure.link("CaseID", "https://www.cnblogs.com/vincent-li666/p/14689449.html");
        String url = "https://gitee.com";
        // new Page
        GiteePage giteePage = new GiteePage(driver);
        // Mark Case Step
        LeoLogger.logCaseStep("open url " + url);
        driver.get(url);

        LeoLogger.logCaseStep("enter search keyword leo-api-auto");
        UIController.text(giteePage.navbar_search_input, "leo-api-auto");

        LeoLogger.logCaseStep("wait 1 s and click search button");
        Common.sleep(1000);
        UIController.click(driver, giteePage.navbar_search_all_link);

        LeoLogger.logCaseStep("assert first result title = Li-Vincent/leo-api-auto");
        String firstTitle = giteePage.search_result_1_title.getText();
        Assert.assertEquals(firstTitle, "Li-Vincent/leo-api-auto");
        ScreenShot.attachScreenShotToAllure(driver);
    }
}
