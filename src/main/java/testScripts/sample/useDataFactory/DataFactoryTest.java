package testScripts.sample.useDataFactory;

import common.utils.LeoLogger;
import io.qameta.allure.Allure;
import org.testng.ITest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * Use Factory and DataProvider to drive test.
 * It is not recommended to use Factory, because the allure report will only
 * display one result for the same test method.
 *
 * @author Vincent-Li
 */
public class DataFactoryTest {

    @DataProvider(name = "factoryProvider")
    public static Object[][] data() {
        return new Object[][]{
                // @formatter:off
                {"Scenario_pass", 1.0d},
                {"Scenario_fail", 2.0d},
                {"Scenario_skip", 3.0d}
                // @formatter:off
        };
    }

    @Factory(dataProvider = "factoryProvider")
    public Object[] createTest(String scenarioName, double data) {
        Object[] tests = new Object[1];
        tests[0] = new FactoryTest(scenarioName, data);
        return tests;
    }
}

class FactoryTest {
    String scenarioName;
    double data;

    public FactoryTest(String scenarioName, double data) {
        this.scenarioName = scenarioName;
        this.data = data;
    }

    @BeforeMethod()
    public void preCondition() {
        LeoLogger.logCaseStep("preCondition scenarioName: " + scenarioName);
        if (scenarioName.equals("Scenario_skip")) {
            throw new IllegalStateException("Config failed.", new UnsupportedOperationException());
        }
    }

    @Test()
    public void testFactory() {
        LeoLogger.logCaseStep("scenarioName: " + scenarioName);
        LeoLogger.logCaseStep("Data: " + data);
        if (scenarioName.equals("Scenario_fail")) {
            throw new IllegalStateException("This Scenario should be failed.", new UnsupportedOperationException());
        }
    }
}