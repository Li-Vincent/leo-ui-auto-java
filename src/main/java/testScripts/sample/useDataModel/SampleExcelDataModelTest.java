package testScripts.sample.useDataModel;

import common.utils.LeoLogger;
import data.dataModel.excel.sample.SampleExcelDataModel;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

public class SampleExcelDataModelTest {
    SampleExcelDataModel data = null;
    static Map<String, SampleExcelDataModel> dataMap = SampleExcelDataModel.read(
            System.getProperty("user.dir") + "/src/main/java/data/dataSource/excel/sample/sampleData.xlsx");

    @DataProvider(name = "fromExcelData")
    public Object[][] dataArray() {
        return new Object[][]{
                {"testName1", "scenario1"},
                {"testName1", "scenario2"},
        };
    }

    @BeforeMethod()
    public void preCondition(Object[] params) {
        LeoLogger.logCaseStep("preCondition scenarioName: " + params[1]);
    }

    @Test(dataProvider = "fromExcelData")
    public void testExcelData(String testName, String scenarioName) {
        this.data = dataMap.get(testName + scenarioName);
        LeoLogger.logCaseStep("testName: " + testName);
        LeoLogger.logCaseStep("scenarioName: " + scenarioName);
        LeoLogger.logCaseStep("data.userName: " + data.getName());
        LeoLogger.logCaseStep("data.nickname: " + data.getNickname());
        LeoLogger.logCaseStep("data.phone: " + data.getPhone());
        LeoLogger.logCaseStep("data.email: " + data.getEmail());
    }

}