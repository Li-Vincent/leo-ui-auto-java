package testScripts.sample.useDataModel;

import common.utils.LeoLogger;
import data.dataModel.yaml.sample.SampleYamlData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class SampleYamlDataModelTest {
    SampleYamlData.SampleModel data;
    static SampleYamlData sampleData = SampleYamlData.getAllDataFromFile(System.getProperty("user.dir")
            + "/src/main/java/data/dataSource/yaml/sample/sampleData.yaml");

    @DataProvider(name = "fromYamlData")
    public Object[][] dataArray() {
        return new Object[][]{
                {"testName1", "scenario1"},
                {"testName1", "scenario2"},
                {"testName2", "scenario1"},
                {"testName2", "scenario2"},
        };
    }


    @BeforeMethod()
    public void preCondition(Object[] params) {
        LeoLogger.logCaseStep("preCondition scenarioName: " + params[1]);
    }

    @Test(dataProvider = "fromYamlData")
    public void testYamlData(String testName, String scenarioName) {
        this.data = SampleYamlData.getDataByScenario(sampleData, testName, scenarioName);
        LeoLogger.logCaseStep("testName: " + testName);
        LeoLogger.logCaseStep("scenarioName: " + scenarioName);
        LeoLogger.logCaseStep("data.userName: " + data.getName());
        LeoLogger.logCaseStep("data.nickname: " + data.getNickname());
        LeoLogger.logCaseStep("data.phone: " + data.getPhone());
        LeoLogger.logCaseStep("data.email: " + data.getEmail());
    }
}