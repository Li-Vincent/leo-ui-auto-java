package testScripts.sample.useDataProvider;

import common.utils.LeoLogger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Iterator;

public class DataProviderTest {
    @DataProvider(name = "arrayProvider")
    public Object[][] dataArray() {
        return new Object[][]{
                {"Scenario_pass", 1.0d},
                {"Scenario_fail", 2.0d},
                {"Scenario_skip", 3.0d}
        };
    }

    @DataProvider(name = "iteratorProvider")
    public Iterator<Object[]> dataIterator() {
        return Arrays.asList(
                new Object[]{"Scenario_pass", 1.0d},
                new Object[]{"Scenario_fail", 2.0d},
                new Object[]{"Scenario_skip", 3.0d}
        ).iterator();
    }

    @BeforeMethod()
    public void preCondition(Object[] params) {
        String scenarioName = (String) params[0];
        LeoLogger.logCaseStep("preCondition scenarioName is " + scenarioName);
        if (scenarioName.equals("Scenario_skip")) {
            throw new IllegalStateException("Config failed.", new UnsupportedOperationException());
        }
    }

    @Test(dataProvider = "arrayProvider")
    public void testProvider(String scenarioName, double data) {
        LeoLogger.logCaseStep("scenarioName: " + scenarioName);
        LeoLogger.logCaseStep("Data: " + data);
        if (scenarioName == "Scenario_fail") {
            throw new IllegalStateException("This Scenario should be failed.", new UnsupportedOperationException());
        }
    }

    @Test(dataProvider = "iteratorProvider")
    public void testProvider2(String scenarioName, double data) {
        LeoLogger.logCaseStep("scenarioName: " + scenarioName);
        LeoLogger.logCaseStep("Data: " + data);
        if (scenarioName == "Scenario_fail") {
            throw new IllegalStateException("This Scenario should be failed.", new UnsupportedOperationException());
        }
    }

}
