package listener;

import common.utils.LeoLogger;

public class TestLeoLogger {
    public static void main(String[] args) {
        LeoLogger.info("info");
        LeoLogger.error("error1");
        LeoLogger.error("error2", new Exception("error2"));
        LeoLogger.warn("warn");
        LeoLogger.trace("trace");
        LeoLogger.debug("debug");
        LeoLogger.debug("debug2", new Exception("debug2"));
        LeoLogger.setTestStep("debug2");
        LeoLogger.setTestStep("aaaaaaaaaaaaa");
        LeoLogger.setTestStep("cccc");
    }
}
