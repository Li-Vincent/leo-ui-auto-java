# 开发日志

### 2020-03-22

项目初始化

### 2020-03-25

确定目录结构，引入必要mvn依赖

### 2020-03-27

创建BaseFlow，添加监听器DefaultBaseFlowListener, 添加失败重跑监听器DefaultRetryListener

### 2020-03-28

添加testBaseFlow测试脚本Sample

    PassFlowTest
    FailFlowTest
    SkipFlowTest
    DependsOnSkipFlowTest
    FailedConfigurationFlowTest

### 2020-03-30

1. 引入Allure报告，添加失败自动截图功能
2. 添加LeoLogger，能够向log文件中输出log
3. 在测试脚本中调用 LeoLogger.logCaseStep("step description") 即可向Allure报告中添加 Step

### 2020-04-03

1. 添加useDataProvider测试脚本Sample
2. 添加useDataFactory测试脚本Sample

### 2020-04-06

1. 添加Excel模块，支持从Excel文件中获取测试数据
2. 定义data目录结构

   data/dataModel/excel 用于存放Excel Model类

   data/dataSource/excel 用于存放Excel 数据文件 .xls/.xlsx
3. 添加Excel Model Sample
4. 添加useDataModel测试脚本 - SampleExcelDataModelTest

### 2020-04-08

1. 添加Yaml模块，支持从Yaml文件中获取测试数据
2. 定义data目录结构

   data/dataModel/yaml 用于存放Yaml Model类

   data/dataSource/yaml 用于存放Yaml 数据文件 .yaml/.yml
3. 添加Yaml Model Sample
4. 添加useDataModel测试脚本 - SampleYamlDataModelTest

### 2020-04-12

1. Page Object模式
2. 添加UIController， 实现一些页面操作方法
3. 添加一个测试脚本Sample, GiteeSearchTest

### TODO

1. 添加教程
2. Support Call HTTP API and get response data, then use data in UI Testing.
3. Support DB Connect to get data from Database. Mongodb and MySQL
